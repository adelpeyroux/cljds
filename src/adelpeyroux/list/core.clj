(ns adelpeyroux.list.core)

(defrecord Cons [head tail])
(defrecord List [data size])

(defn size
  "Returns the size of the list in constant time."
  [list]
  (:size list))

(defn empty-list?
  "Returns true if list is empty, false othewise."
  [list]
  (= (size list) 0))

(defn empty-list
  "Returns an empty list."
  []
  (List. nil 0))

(defn head
  "Returns the first elmeent into the list."
  [list]
  (if (nil? (:data list))
    nil
    (-> list :data :head)))

(defn tail
  "Returns the list that begins at the second element of the given list."
  [list]
  (if (nil? (:data list))
    (empty-list)
    (List. (-> list :data :tail) (dec (:size list)))))

(defn at
  "Return the element at the given index. It returns nil if index in invalid."
  [list index]
  (cond
    (empty-list? list) nil
    (zero? index) (head list)
    :else (recur (tail list) (dec index))))

(defn list-last
  "Return the last element of the list."
  [list]
  (at list (- (size list) 1)))

(defn list-contains?
  "Checks if a vlue is in the list."
  [list value]
  (cond
    (empty-list? list) false
    (= (head list) value) true
    :else (recur (tail list) value)))

(defn cons-list
  "Add the given element at the begining of the given list."
  [value list]
  (List. (Cons. value (:data list)) (inc (:size list))))

(defn reverse-list
  "Returns the reversed list."
  [list]
  (let [tail-func (fn [list acc] (if (empty-list? list)
                                   acc
                                   (recur (tail list) (cons-list (head list) acc))))]
    (tail-func list (empty-list))))

(defn list-insert
  "Inserts given value into the list at the given index. If the index is invalid, the list is returned unmodified."
  [value index list]
  (let [tail-recur (fn [value index list acc]
                     (cond
                       (empty-list? list) (reverse-list acc)
                       (zero?  index) (recur value (dec index) list (cons-list value acc))
                       :else (recur value (dec index) (tail list) (cons-list (head list) acc))))]
    (tail-recur value index list (empty-list))))
