(ns adelpeyroux.cljds-list-test
  (:require [adelpeyroux.list.core :as l]
            [clojure.test :refer [deftest is testing]]))

(def _empty (l/empty-list))
(def _list (l/cons-list 1 (l/cons-list 2 (l/cons-list 3 _empty))))

(deftest empty-list?-test
  (testing "List empty-list?"
    (is true (l/empty-list? empty))))

(deftest head-test
  (testing "List head"
    (is 1 (l/head _list))))

(deftest tail-test
  (testing "List tail"
    (is 2 (l/size (l/tail _list)))))

(deftest reverse-list-test
  (testing "List reverse-list"
    (let [reverted (l/reverse-list _list)]
      (is 3 (l/head reverted)))))

(deftest insert-list-test
  (testing "List insert-list"
    (let [inserted (l/list-insert 4 2 _list)]
      (is (and
           (= 4 (l/size inserted))
           (l/list-contains? inserted 4)
           (= 1 (l/head inserted)))))))

(deftest insert-list-ko-test
  (testing "List insert-list"
    (let [inserted (l/list-insert 4 4 _list)]
      (is (and
           (= 3 (l/size inserted))
           (not (l/list-contains? inserted 4))
           (= 1 (l/head inserted)))))))

(deftest at-ok-test
  (testing "List at - OK"
    (is 2 (l/at list 1))))

(deftest at-ko-test
  (testing "List at - KO"
    (is (nil? (l/at list 4)))))

(deftest last-test
  (testing "List list-last"
    (is 3 (l/list-last _list))
    (is (nil? (l/list-last _empty)))))
